
from pandas_wrappers import *
from scipy.interpolate import griddata
import pandas as pd
import matplotlib.pyplot as plt

import cartopy
import cartopy.crs as ccrs
from cartopy.io.img_tiles import OSM

import utm


if __name__ == '__main__':

    df = pd.read_csv('ofcom_broadband_2013.csv')
    points = np.array(df[['longitude', 'latitude']])
    values = np.array(df[['connection']])
    
    for px in range(points.shape[0]):
        pu = utm.from_latlon(points[px, 1], points[px, 0])
        points[px, 0] = pu[0]
        points[px, 1] = pu[1]


    frame = get_cartopy_extent(df)
    

    
    X, Y = np.meshgrid(
            np.linspace(np.min(points[:,0]), np.max(points[:,0]), 100),
            np.linspace(np.min(points[:,1]), np.max(points[:,1]), 100),
    )

    Z = griddata(points, values, (X, Y), method='nearest')
 

    imagery = OSM()
    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1, projection=imagery.crs)

    #ax.add_image(imagery, 14)

    #ax.set_extent(bath_extent, ccrs.PlateCarree())
    c = ax.pcolormesh(X, Y, Z[:,:, 0], transform=ccrs.OSGB(), zorder=10, alpha=0.5)
    fig.colorbar(c, ax=ax)



    plt.show()























