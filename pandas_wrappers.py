import numpy as np

import pandas as pd

bath_extent = (-2.413775, -2.318295, 51.413590, 51.349905)

def get_rubbish():
    #df = pd.read_pickle('./fms.pkl')
    df = pd.read_csv('./fms.csv')
    return df[df.category == 'Rubbish (refuse and recycling)']

def get_cartopy_extent(df):
    
    long_min = np.min(df.longitude)
    long_max = np.max(df.longitude)
    
    long_center = np.average([long_min, long_max])
    left = long_center - (long_center - long_min)*1.05
    right = long_center + (long_max - long_center)*1.05
    
    lat_min = np.min(df.latitude)
    lat_max = np.max(df.latitude)
    
    lat_center = np.average([lat_min, lat_max])
    bottom = lat_center - (lat_center - lat_min)*1.05
    top = lat_center + (lat_max - lat_center)*1.05
    
    return (left, right, bottom, top)








