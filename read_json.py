

import json
import sys



def load_json(filename):
    with open(filename) as fh:
        raw_json = fh.read()

    data = json.loads(raw_json)
    return data


if __name__ == '__main__':

    filename = sys.argv[1]
    data = load_json(filename)
    



