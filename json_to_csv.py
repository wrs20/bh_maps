import csv
import json
import sys


filename = sys.argv[1]

data_json = json.load(open(filename, 'r'))

headers = data_json['features'][0]['properties'].keys()

filename = filename.split('.')[:-1] + ['csv']
filename = '.'.join(filename)

with open(filename, 'w') as fh:
    writer = csv.DictWriter(fh, headers)
    
    writer.writeheader()
    for data in data_json['features']:
        writer.writerow(data['properties'])
        
