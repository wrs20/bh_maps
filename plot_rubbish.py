import json


from matplotlib.path import Path
import matplotlib.pyplot as plt
import numpy as np

import cartopy
import cartopy.crs as ccrs
from cartopy.io.img_tiles import OSM

from pandas_wrappers import *

def tube_locations():
    """
    Return an (n, 2) array of selected London Tube locations in Ordnance
    Survey GB coordinates.

    Source: http://www.doogal.co.uk/london_stations.php

    """
    return np.array([[531738., 180890.], [532379., 179734.],
                     [531096., 181642.], [530234., 180492.],
                     [531688., 181150.], [530242., 180982.],
                     [531940., 179144.], [530406., 180380.],
                     [529012., 180283.], [530553., 181488.],
                     [531165., 179489.], [529987., 180812.],
                     [532347., 180962.], [529102., 181227.],
                     [529612., 180625.], [531566., 180025.],
                     [529629., 179503.], [532105., 181261.],
                     [530995., 180810.], [529774., 181354.],
                     [528941., 179131.], [531050., 179933.],
                     [530240., 179718.]])



def locations_by_key(filename, key):
    
    with open(filename) as fh:
        raw = fh.read()








if __name__ == '__main__':
    imagery = OSM()

    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1, projection=imagery.crs)

    # Add the imagery to the map.
    ax.add_image(imagery, 14)

    df = get_rubbish()
    df.plot.scatter(x='easting', y='northing', transform=ccrs.OSGB(), ax=ax)
    extent = get_cartopy_extent(df)
    bath_extent = (-2.413775, -2.318295, 51.413590, 51.349905)
    ax.set_extent(extent, ccrs.PlateCarree())
    
    ax.set_title('Bath rubbish locations')
    plt.show()








